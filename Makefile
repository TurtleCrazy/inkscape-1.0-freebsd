# Created by: Alexander Nedotsukov <bland@FreeBSD.org>
# $FreeBSD: head/graphics/inkscape/Makefile 533681 2020-05-02 10:00:12Z tcberner $

PORTNAME=	inkscape
PORTVERSION=	1.0
CATEGORIES=	graphics gnome
MASTER_SITES=https://inkscape.org/gallery/item/18460/

MAINTAINER=	gnome@FreeBSD.org
COMMENT=	Full featured open source SVG editor

LICENSE=	GPLv2 GPLv2+ GPLv3 GPLv3+ LGPL21 LGPL3 LGPL3+ MPL11
LICENSE_COMB=	multi
LICENSE_FILE_GPLv2  =	${WRKSRC}/LICENSES/GPL-2.0.txt
LICENSE_FILE_GPLv2+ =	${WRKSRC}/LICENSES/GPL-2.0-or-later.txt
LICENSE_FILE_GPLv3  =	${WRKSRC}/LICENSES/GPL-3.0.txt
LICENSE_FILE_GPLv3+ =	${WRKSRC}/LICENSES/GPL-3.0-or-later.txt
LICENSE_FILE_LGPL21 =	${WRKSRC}/LICENSES/LGPL-2.1.txt
LICENSE_FILE_LGPL3  =	${WRKSRC}/LICENSES/LGPL-3.0.txt
LICENSE_FILE_LGPL3+ =	${WRKSRC}/LICENSES/LGPL-3.0-or-later.txt
LICENSE_FILE_MPL11  =	${WRKSRC}/LICENSES/MPL-1.1.txt

LIB_DEPENDS=	libdouble-conversion.so:devel/double-conversion \
	libsigc-2.0.so:devel/libsigc++20 \
	libgdl-3.so:x11-toolkits/gdl \
	libgc.so:devel/boehm-gc \
	libgsl.so:math/gsl \
	libpotrace.so:graphics/libpotrace \
	libsoup-2.4.so:devel/libsoup \
	libharfbuzz.so:print/harfbuzz \
	libfontconfig.so:x11-fonts/fontconfig \
	libpng.so:graphics/png \
	libfreetype.so:print/freetype2 \
	libenchant-2.so:textproc/enchant2
RUN_DEPENDS=	${PYNUMPY} \
		${PYTHON_PKGNAMEPREFIX}lxml>0:devel/py-lxml@${PY_FLAVOR} \
		${PYTHON_PKGNAMEPREFIX}scour>0:textproc/py-scour@${PY_FLAVOR}
BUILD_DEPENDS=	${LOCALBASE}/include/boost/concept_check.hpp:devel/boost-libs

USES=	compiler:c++11-lib cmake \
		iconv:wchar_t jpeg pathfix pkgconfig python:3.5+ \
		shebangfix tar:xz xorg \
		desktop-file-utils \
		gnome
USE_GNOME=	gtkmm30 libxml2 libxslt cairo pango
USE_XORG=	x11 xext ice sm

WRKSRC=${WRKDIR}/${PORTNAME}-1.0_2020-05-01_4035a4fb49

USE_LDCONFIG=	yes

INSTALLS_ICONS=	yes

SHEBANG_FILES=	\
		share/extensions/genpofiles.sh \
		man/fix-roff-punct

OPTIONS_DEFINE=	POPPLER VISIO CDR DBUS WPG LCMS SVG2 OPENMP  NLS GTKSPELL
OPTIONS_DEFAULT=POPPLER VISIO CDR WPG LCMS SVG2 IMAGEMAGICK NLS GTKSPELL
OPTIONS_DEFAULT_amd64=	OPENMP
OPTIONS_SUB=	yes

OPTIONS_SINGLE=	MAGICK
OPTIONS_SINGLE_MAGICK=	IMAGEMAGICK GRAPHICSMAGICK

POPPLER_DESC=		PDF preview rendering
POPPLER_CMAKE_BOOL=	ENABLE_POPPLER ENABLE_POPPLER_CAIRO
POPPLER_LIB_DEPENDS=	libpoppler.so:graphics/poppler \
			libpoppler-glib.so:graphics/poppler-glib

VISIO_DESC=		Support for Microsoft Visio diagrams
VISIO_CMAKE_BOOL=	WITH_LIBVISIO
VISIO_LIB_DEPENDS=	librevenge-stream-0.0.so:textproc/librevenge \
			libvisio-0.1.so:textproc/libvisio01

CDR_DESC=		Support for CorelDRAW diagrams
CDR_CMAKE_BOOL=		WITH_LIBCDR
CDR_LIB_DEPENDS=	librevenge-stream-0.0.so:textproc/librevenge \
			libcdr-0.1.so:graphics/libcdr01

DBUS_DESC		=	Support for DBus interface
DBUS_CMAKE_BOOL=	WITH_DBUS
DBUS_LIB_DEPENDS=	libdbus-glib-1.so:devel/dbus-glib \
			libdbus-1.so:devel/dbus

WPG_DESC=		Support for WordPerfect graphics
WPG_CMAKE_BOOL=		WITH_LIBWPG
WPG_LIB_DEPENDS=	libwpg-0.3.so:graphics/libwpg03

SVG2_DESC=		Support for new SVG2 features
SVG2_CMAKE_BOOL=		WITH_LIBWPG
SVG2_USE=			GNOME=librsvg2,cairo

LCMS_DESC=			LCMS support
LCMS_CMAKE_BOOL=	ENABLE_LCMS
LCMS_LIB_DEPENDS=	liblcms2.so:graphics/lcms2

GTKSPELL_DESC=		Support of GTK spell
GTKSPELL_CMAKE_BOOL=	WITH_GTKSPELL
GTKSPELL_LIB_DEPENDS=	libgtkspell3-3.so:textproc/gtkspell3

OPENMP_DESC=		OpenMP support
OPENMP_USES=		compiler:openmp
OPENMP_USES_OFF=	compiler:c++11-lib
OPENMP_CMAKE_BOOL=	WITH_OPENMP

IMAGEMAGICK_DESC=	ImageMagick6
IMAGEMAGICK_LIB_DEPENDS=	libMagick++-6.so:graphics/ImageMagick6
IMAGEMAGICK_CMAKE_BOOL=WITH_IMAGE_MAGICK

GRAPHICSMAGICK_DESC=	GraphicsMagick
GRAPHICSMAGICK_LIB_DEPENDS=	libGraphicsMagick++.so:graphics/GraphicsMagick
GRAPHICSMAGICK_CMAKE_BOOL=WITH_GRAPHICS_MAGICK

NLS_DESC	=	Native Language Support
NLS_CMAKE_BOOL=		WITH_NLS
NLS_USES=		gettext

post-patch:
	@${REINPLACE_CMD} -e 's|web-set-att|web_set_att|g' \
		${WRKSRC}/po/*.po
	@${REINPLACE_CMD} -e 's|web-transmit-att|web_transmit_att|g' \
		${WRKSRC}/po/*.po
	@${REINPLACE_CMD} -e 's|COMMAND python3|COMMAND ${PYTHON_VERSION}|g' \
 		${WRKSRC}/share/symbols/CMakeLists.txt \
 		${WRKSRC}/share/templates/CMakeLists.txt \
 		${WRKSRC}/share/filters/CMakeLists.txt \
 		${WRKSRC}/share/paint/CMakeLists.txt \
 		${WRKSRC}/share/palettes/CMakeLists.txt

.include <bsd.port.mk>
