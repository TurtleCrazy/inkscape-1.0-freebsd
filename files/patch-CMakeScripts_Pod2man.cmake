--- CMakeScripts/Pod2man.cmake.orig	2020-05-01 13:17:41 UTC
+++ CMakeScripts/Pod2man.cmake
@@ -27,6 +27,7 @@ macro(pod2man PODFILE_FULL RELEASE SECTION CENTER)
     if(NOT EXISTS ${PODFILE_FULL})
         message(FATAL ERROR "Could not find pod file ${PODFILE_FULL} to generate man page")
     endif(NOT EXISTS ${PODFILE_FULL})
+	message(STATUS "pod2man ${CMAKE_CURRENT_SOURCE_DIR}" )
 
     if(POD2MAN)
         if(LANG)
@@ -34,13 +35,13 @@ macro(pod2man PODFILE_FULL RELEASE SECTION CENTER)
             set(MANFILE_TEMP "${CMAKE_CURRENT_BINARY_DIR}/${NAME}.${LANG}.tmp")
             set(MANFILE_FULL "${CMAKE_CURRENT_BINARY_DIR}/${NAME}.${LANG}.${SECTION}")
             set(MANFILE_FULL_GZ "${CMAKE_CURRENT_BINARY_DIR}/${NAME}.${LANG}.${SECTION}.gz")
-            set(MANFILE_DEST "${SHARE_INSTALL}/man/${LANG}/man${SECTION}")
+            set(MANFILE_DEST "man/${LANG}/man${SECTION}")
         else()
             set(MANPAGE_TARGET "man-${NAME}")
             set(MANFILE_TEMP "${CMAKE_CURRENT_BINARY_DIR}/${NAME}.tmp")
             set(MANFILE_FULL "${CMAKE_CURRENT_BINARY_DIR}/${NAME}.${SECTION}")
             set(MANFILE_FULL_GZ "${CMAKE_CURRENT_BINARY_DIR}/${NAME}.${SECTION}.gz")
-            set(MANFILE_DEST "${SHARE_INSTALL}/man/man${SECTION}")
+            set(MANFILE_DEST "man/man${SECTION}")
         endif()
         add_custom_command(
             OUTPUT ${MANFILE_TEMP}
@@ -50,7 +51,7 @@ macro(pod2man PODFILE_FULL RELEASE SECTION CENTER)
 
         add_custom_command(
             OUTPUT ${MANFILE_FULL}
-            COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/fix-roff-punct "${MANFILE_TEMP}" > ${MANFILE_FULL}
+			COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/fix-roff-punct "${MANFILE_TEMP}" > ${MANFILE_FULL}
             DEPENDS ${MANFILE_TEMP}
         )
         if(GZIP)
